// <auto-generated />
namespace Vidly2.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class UpdatedFiles : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UpdatedFiles));
        
        string IMigrationMetadata.Id
        {
            get { return "202008260131533_UpdatedFiles"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
