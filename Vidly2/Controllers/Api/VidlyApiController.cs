﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Vidly2.App_Start;
using Vidly2.Models;

namespace Vidly2.Controllers.Api
{
    public class VidlyApiController : ApiController
    {
        protected readonly MapperConfiguration _mapperConfig;
        protected readonly IMapper _mapper;
        protected readonly VidlyContext _context;

        public VidlyApiController()
        {
            _context = new VidlyContext();
            _mapperConfig = new AutoMapperConfig().Configure();
            _mapper = _mapperConfig.CreateMapper();
        }

        protected void ThrowValidationErrors()
        {
            StringBuilder errorStrBuilder = new StringBuilder();

            foreach (var modelState in ModelState.Values)
                foreach (ModelError error in modelState.Errors)
                    errorStrBuilder.Append(error.ErrorMessage + "\n");

            var errorResponse = Request.CreateErrorResponse(HttpStatusCode.BadRequest, errorStrBuilder.ToString());
            throw new HttpResponseException(errorResponse);
        }
    }
}