﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Vidly.Models.Configurations
{
    public class MovieConfiguration : EntityTypeConfiguration<Movie>
    {
        public MovieConfiguration()
        {
            HasRequired(g => g.Genre)
                .WithMany(g => g.Movies)
                .HasForeignKey<int?>(m => m.GenreId)
                .WillCascadeOnDelete(false);

        }
    }
}